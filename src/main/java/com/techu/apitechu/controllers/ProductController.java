package com.techu.apitechu.controllers;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;

@RestController
public class ProductController {

    static final String ApiBaseUrl = "/apitechu/v1";

    @GetMapping(ApiBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");
        // return new ArrayList<>();
        return ApitechuApplication.productModels;
    }

    @GetMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById: " + id);
        ProductModel rest = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                rest = product;
            }
        }

        return rest;
    }

    @PostMapping(ApiBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct: " + newProduct.getId());
        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct: " + product.getId());
        System.out.println("updateProduct: " + id);

        for (ProductModel prod : ApitechuApplication.productModels) {
            if (prod.getId().equals(id)) {
                prod.setId(product.getId());
                prod.setDesc(product.getDesc());
                prod.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @DeleteMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct: " + id);
        ProductModel product = new ProductModel();
        boolean indicador = false;
        for (ProductModel productModel : ApitechuApplication.productModels) {
            if (productModel.getId().equals(id)) {
                System.out.println("Existe");
                indicador = true;
                product = productModel;
            }
        }
        System.out.println("indicador: " + indicador);
        if (indicador) {
            System.out.println("Id delete:" + product.getId());
            ApitechuApplication.productModels.remove(product);
        }
        return product;
    }
    /*
    * Pathc, no se permite actualizar id
    * Solo Actualizar precio o descripción
    * Envio una actualización error debe mantenerse igual
    * precio o descripcion resultado objeto
    * */

    @PatchMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("patchProduct: " + id);
        boolean indicador = false;

        //ProductModel student = ApitechuApplication.findById(id);

        ProductModel result = new ProductModel();
        for (ProductModel productModel : ApitechuApplication.productModels) {
            if (productModel.getId().equals(id)) {
                System.out.println("Existe");
                result = productModel;
                //productModel.setId(id);
                if(product.getDesc() != null)
                    productModel.setDesc(product.getDesc());

                if(product.getPrice()>0.0)
                    productModel.setPrice(product.getPrice());

               // product = productModel;
                break;
            }
        }

        return result;
    }
}
